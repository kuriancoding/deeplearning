## Restricted Boltzman machines

### Energy based systems
They are learning systems which have an energy function as their
parameter to optimise.
Energy functions have some properties
- they are differentiable at all points
- they are positive at all points
- they always converge to finite value over finite space

the observable part of the machine learning nodes is the one which 
measures pixels, i.e. the one which checks if a given node is on
or off. Those nodes which detect features like edges, rectangles,
faces are called hidden nodes.

The energy function is given as 

E=summation(x.x<sub>bias</sub>)+summation(y.y<sub>bias</sub>)+summation(x.y.connected_wieght<sub>xy</sub>)

probablity function is p(x)=exp(-E)/Z
