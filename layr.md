## What is deep learning?
deep learning is extracting of meaning from a given set of data
which goes beyond simple mechanincal inferences.

## How does this happen?
A machine can be trained to *observe* a data for a feature, like
in a voice data it can look for gender,ethinicity,age. 

In a photo it can look for things like "happy face".

this happens because instead of taking a binary yes/no answer from 
the computer, we take the summation of responses of a group
of nodes after normalizing them.

the nodes that are at the same distance from either the input
or output are said to belong to the same *layer*.

think of a memmory foam pillow that fits the shape of someone's head.
Each layer can be approximately thought of as a memmory foam pillow.

## How are different neural networks different from one another?
the way one method differs from another layer is the way in 
which each node of the network computes its output.

## Why are layers required?

At each node the [complexity of computation is
limited](https://stats.stackexchange.com/questions/63152/what-does-the-hidden-layer-in-a-neural-network-compute). For example
in a neural network that works on sound signals, the bottom most
layer may detect high pitches or bases. The existence of a certain
pitch on the other hand is not an inference into if the speaker is
agitated or happy.

A layer on top of this base layer will tell if the speaker is male or
female(since the voice signatures can be different for both).

A layer on top of the second layer can tell if the speaker is old or
young. 

A layer on top of the third layer will finally collect all the
information from the preceeding layers to decide if the speaker is happy
or agitated.

Compressing all this analysis into one layer will lead to higher
complexity and lesser reliance, because the functional complexity of the
layer has increased.

Also having a separate layer makes it easy to correct the outliers
through a better targeted feed back loop(i.e. there is a separate feedback
loop for pitch detection and a different one for age).

What is the difference between hidden and visible nodes?

The hidden layers work is to transform the input data into a measurable
parameter at the output. Some nodes of the hidden layer are connected to 
the output and some are not, the nodes which are not connected are
called bias nodes.  Bias nodes help in fitting the curve by shifting the
output along one of the independent axes.

*NOTE*: all this is still one dimensional

## Markov Process and Markov Random Field
### what is a Markov random field?
It is a set of random variables that satisfy the Markov property

### what is Markov property?

A process which depends only on the value of the present random variable
s to determine the value of the future is known as a Markov process and 
processes which satisfy this condition are said to satisfy the Markov
property.




